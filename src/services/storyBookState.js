App = App || {};
App["services"] = App["services"] || {};
var tableName = "storybookstate";
var initialised = false;
App["services"]["storyBookState"] = function (){
    console.log("initing storybook state service");
    function processState(){

    }



    var self = {
        "store":undefined,
        "saveUpdateItem": function ( event, item){
           if(! item.itemid){
               item.itemid = App.items.length;
               App.items.push(item);
           }else{
               App.items[item.itemid] = item;
           }
        },
        "clear": function (){
          self.store.nuke();
        },
        saveUpdateActivePage : function (activePage, cb){
          self.store.save({"key":"activePage","value":activePage},cb);
        },
        loadActivePage : function (cb){
          self.store.get("activePage",cb);
        },
        saveUpdatePage : function (pageNum, page, cb){
          self.store.get("pages",function (data){
             console.log("saveUpdatePage", data, page);
             var pages;
             if(data){
                 pages = data.value;
             }else{
                 pages = [];
             }
             pages.push(pageNum);
             console.log("pages b4 save ", pages);

             self.store.save({"key":"pages","value":pages});
             self.store.save({"key":pageNum,"value":page},cb);
          });
        },
        "loadPages":function (cb){
            //self.store.nuke();
          self.store.get("pages",function (data){
             var pages = {};
             if(data){
               console.log("pages from loadPages",data);
               var parallel = [];
               var ps =data.value;
               for(var i=0; i<ps.length; i++){
                  var pagnum = ps[i];
                  parallel.push(function (callback){
                      console.log("get page " + pagnum);
                      self.store.get(pagnum, function (page){
                          console.log("got page", page);
                        pages[pagnum] = page.value;
                        callback();
                      });
                  });
               }
               async.parallel(parallel, function (err, ok){
                   cb(pages);
               });
             }else{
                 cb();
             }
          });
        },
        "loadState": function (cb){
            //load the state from db to mem
            if(initialised)return cb(undefined,App.storybook.state);
            self.store.get("state", function (data){
               App.storybook.state = data;
               cb(undefined,data);
            });

        }
    };

    $('document').ready(function (){
        $(App).on('itemdrop',self.saveUpdateItem);
    });

    return function (readyCb){
        Lawnchair({"name": tableName}, function (store) {
            self.store = store;
            self.loadState(function (err, data){
                if(err) readyCb(err);
                else{
                    readyCb(undefined,self);
                }
            });
        });
    };



}();
