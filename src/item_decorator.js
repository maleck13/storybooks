//        ele.mousedown(function(event) {
//            ox = event.screenX;
//            oy = event.screenY;
//            osx = ox;
//            osy = oy;
//            ele.attr({
//                opacity: .5
//            });
//            dragging = true;
//            var item_height = ele.item_height;
//            ele.paper.setSize($(window).width(), $(window).height() + (item_height * idx));
//            ele.ody = 0;
//            ele.odx = 0;
//            ele.item_height = item.height();
//
//
//            if (idx > 0 && !ele.adj) {
//                ele.adj = true;
//                ele.paper.canvas.style.top = "-" + (item_height * idx);
//                ele.ody = ele.ody + (item_height * idx);
//            }
//            ele.translate(ele.odx, ele.ody);
//
//            ele.paper.canvas.style.zIndex = 100000;
//        });
//
//        ele.mousemove(function(event) {
//            if (dragging) {
//
//                ele.translate(event.screenX - ox, event.screenY - oy);
//
//                ox = event.screenX;
//                oy = event.screenY;
//
//
//
//                if(paperOffset.left < event.pageX){
//
//                    console.log("on paper", event);
//                }
//
//            }
//        });
//
//        ele.mouseup(function(event) {
//
//            dragging = false;
//            ele.attr({
//                opacity: 1
//            });
//            var returnto = ele.paper.returnTo;
//            ele.paper.setSize(returnto.width, returnto.height);
//            var nset = App.active_page.set();
//            App.active_page.importSVG(importPath,nset);
//
//            var ft = enableItemControls(nset);
//
//            var boxX = nset.getBBox().x;
//            var boxW = nset.getBBox().width;
//            var boxH = nset.getBBox().height;
//            var boxY = nset.getBBox().y;
//
//            var left = (event.pageX - paperOffset.left) - boxX - boxW / 2;
//            var top = (event.pageY - paperOffset.top) - boxY  - boxH / 2;
//             App.active_page.rect(left,top, 20,20);
//            ft.attrs.translate.x = left;
//            ft.attrs.translate.y = top;
//
//            ft.apply();
//            $('a#'+type).trigger('click');
//
//        });
function item_button(e) {

    var item = $(this).data("item");
    var choices = $(this).data("choices").split(",");

    $('.item_choice').each(function (idx, b) {
        b = $(b);
        if (b.data("item") !== item) {
            b.removeClass("btn-primary");
            b.addClass("btn-info");
        } else {
            b.removeClass('btn-info');
            b.addClass("btn-primary");
        }
    });
    $('.item_id').remove();
    $('.item_switch').each(function (idx,ele){
        if(choices[idx]){
            $(this).html(choices[idx])
        }
    });
    $('#item_switch').show();

    var numElements = Math.floor($(window).height() / 150);
    console.log("num",numElements);
    for (var i = 0; i < numElements; i++) {
        $('.item_list').append("<div class='span12 item_id' id=\"" + i + "\" style='z-index:1000000; height: 150px; padding: 0px; margin: 0px; background-color: blueviolet; border-bottom: 1px solid blanchedalmond;'></div>")
    }



    if (item == "sky") {
        var skyItems = items[item];
        $('.item_id').each(function (idx, it){
            addItem(idx,it,item);
        });
    }


}

var importPath = '<svg width="640" height="480" xmlns="http://www.w3.org/2000/svg">'+
    '<defs>'+
    '<linearGradient id="svg_10">'+
    '<stop offset="0" stop-color="#7297a0"/>'+
    '<stop offset="1" stop-color="#8d685f"/>'+
    '</linearGradient>'+
    '</defs>'+
    '<g>'+
    '<path id="svg_1" d="m274.03931,151.57637c-10.83893,0 -20.14229,5.62106 -24.69432,13.73366c-1.81908,-1.01546 -3.82491,-1.62943 -5.96591,-1.62943c-6.47461,0 -11.86096,5.20724 -13.52679,12.27859c-2.84206,-1.1227 -6.14702,-1.80377 -9.67686,-1.80377c-10.58987,0 -19.18042,5.79349 -19.18042,12.93681c0,6.44682 7.04541,11.74886 16.19749,12.72868c-0.41014,0.69646 -0.72913,1.38615 -0.72913,2.11523c0,6.84038 18.73708,12.41835 41.83055,12.41835c23.0907,0 41.82663,-5.57797 41.82663,-12.41835c0,-0.45993 -0.28473,-0.87071 -0.45038,-1.31902c1.01053,0.25371 2.0127,0.52287 3.12299,0.52287c6.17551,0 11.20184,-4.19858 11.20184,-9.36646c0,-5.166 -5.02634,-9.36372 -11.20184,-9.36372c-0.75867,0 -1.40244,0.26155 -2.11615,0.38225c0.62442,-2.06726 1.04034,-4.17453 1.04034,-6.41711c0,-13.67719 -12.38223,-24.7986 -27.67804,-24.7986l0,0z" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="5" stroke="#7297a0" fill="url(#svg_10)"/>'+
    '<path transform="rotate(31.2337 195.719 248.199)" id="svg_2" d="m194.26472,254.26575c-1.75294,-0.65886 -3.00958,-2.752 -2.72414,-4.53786c0.18011,-1.12592 3.97919,-7.96191 4.31087,-7.75681c0.10829,0.06665 1.07196,1.66052 2.1411,3.5416c1.71327,3.01312 1.94456,3.57489 1.94456,4.72321c0,2.32742 -1.86264,4.23376 -4.09895,4.1953c-0.65262,-0.01112 -1.36078,-0.08553 -1.57344,-0.16544l0,0zm1.75632,-1.17084c0.05794,-0.30133 -0.07735,-0.44827 -0.41359,-0.44827c-0.76569,0 -1.82771,-0.96965 -2.06517,-1.88528c-0.25311,-0.97713 -0.92418,-1.09315 -1.02336,-0.17732c-0.19102,1.76466 3.18037,4.18195 3.50212,2.51088l0,0z" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="5" stroke="#1a7477" fill="#7297a0"/>'+
    '<path transform="rotate(25.0604 224.21 256.629)" id="svg_3" d="m222.86821,262.22919c-1.61827,-0.60791 -2.77808,-2.54041 -2.5144,-4.18869c0.16626,-1.03934 3.67291,-7.34952 3.97925,-7.16019c0.09979,0.06189 0.98932,1.53296 1.97662,3.26917c1.5813,2.78137 1.7948,3.29993 1.7948,4.36011c0,2.14819 -1.71936,3.9079 -3.78387,3.87231c-0.60242,-0.01025 -1.25592,-0.0791 -1.45239,-0.15271l0,0zm1.6214,-1.08069c0.05347,-0.27795 -0.07144,-0.41382 -0.38177,-0.41382c-0.7068,0 -1.68713,-0.89502 -1.90614,-1.74054c-0.23381,-0.90167 -0.85344,-1.00864 -0.94481,-0.16339c-0.17633,1.62891 2.93555,3.86023 3.23273,2.31775l0,0z" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="5" stroke="#1a7477" fill="#7297a0"/>'+
    '<path transform="rotate(21.7874 251.602 232.515)" id="svg_4" d="m250.09485,238.80351c-1.81714,-0.6828 -3.11971,-2.8526 -2.82376,-4.7038c0.18663,-1.1673 4.12482,-8.25375 4.46869,-8.04103c0.11234,0.06921 1.11113,1.72142 2.21965,3.67136c1.77594,3.12355 2.01572,3.70583 2.01572,4.89619c0,2.4126 -1.93085,4.38876 -4.24924,4.34894c-0.67648,-0.01154 -1.41051,-0.08871 -1.63106,-0.17166l0,0zm1.82071,-1.21344c0.06015,-0.31242 -0.0802,-0.46472 -0.4286,-0.46472c-0.79376,0 -1.89473,-1.00525 -2.14082,-1.95447c-0.26244,-1.0128 -0.95801,-1.1331 -1.06087,-0.18375c-0.19803,1.82922 3.29672,4.33507 3.63029,2.60294l0,0z" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="5" stroke="#1a7477" fill="#7297a0"/>'+
    '<path transform="rotate(32.8046 267.259 258.972)" stroke="#1a7477" id="svg_5" d="m266.0578,265.99738c-1.44873,-0.76294 -2.48694,-3.18695 -2.25092,-5.25485c0.14844,-1.30405 3.28784,-9.22072 3.56195,-8.98323c0.08978,0.07747 0.88583,1.92325 1.7695,4.10147c1.41568,3.48949 1.60681,4.14027 1.60681,5.47011c0,2.6951 -1.53915,4.90292 -3.38727,4.85828c-0.53946,-0.01288 -1.12448,-0.09924 -1.30008,-0.19177l0,0zm1.45139,-1.35559c0.04773,-0.34888 -0.06393,-0.51917 -0.34177,-0.51917c-0.63297,0 -1.51025,-1.12302 -1.70667,-2.18344c-0.20908,-1.13147 -0.76346,-1.26599 -0.84576,-0.20511c-0.15765,2.04337 2.62817,4.84262 2.8942,2.90771l0,0z" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="5" fill="#7297a0"/>'+
    '<path transform="rotate(27.3499 292 230)" id="svg_8" d="m290.49274,236.28864c-1.81714,-0.6828 -3.11972,-2.8526 -2.82376,-4.7038c0.18661,-1.1673 4.12482,-8.25375 4.46869,-8.04103c0.11234,0.06921 1.11111,1.72142 2.21964,3.67136c1.77594,3.12355 2.01572,3.70583 2.01572,4.89619c0,2.4126 -1.93085,4.38876 -4.24924,4.34894c-0.67648,-0.01154 -1.41049,-0.08871 -1.63104,-0.17166l0,0zm1.82071,-1.21344c0.06015,-0.31242 -0.0802,-0.46472 -0.42862,-0.46472c-0.79376,0 -1.89471,-1.00525 -2.14081,-1.95447c-0.26245,-1.0128 -0.95801,-1.1331 -1.06088,-0.18375c-0.19803,1.82922 3.29672,4.33507 3.63031,2.60294l0,0z" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="5" stroke="#1a7477" fill="#7297a0"/>'+
    '<path transform="rotate(16.1892 210 226)" id="svg_9" d="m208.49274,232.28864c-1.81714,-0.6828 -3.11972,-2.8526 -2.82376,-4.7038c0.18661,-1.1673 4.12482,-8.25375 4.46869,-8.04103c0.11234,0.06921 1.11111,1.72142 2.21964,3.67136c1.77594,3.12355 2.01572,3.70583 2.01572,4.89619c0,2.4126 -1.93085,4.38876 -4.24924,4.34894c-0.67648,-0.01154 -1.41049,-0.08871 -1.63104,-0.17166l0,0zm1.82071,-1.21344c0.06015,-0.31242 -0.0802,-0.46472 -0.42862,-0.46472c-0.79376,0 -1.89471,-1.00525 -2.14081,-1.95447c-0.26245,-1.0128 -0.95801,-1.1331 -1.06088,-0.18375c-0.19803,1.82922 3.29672,4.33507 3.63031,2.60294l0,0z" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="5" stroke="#1a7477" fill="#7297a0"/>'+
    '</g>'+
    '</svg>';

function addItem (idx, e, type) {
    var item = $(e);

    var it = (idx % 2) ? App.items[type][idx] : undefined;
    var page = Raphael(document.getElementById(item.attr("id")), item.width(), item.height());
    page.returnTo = {"height": item.height(), "width": item.width()};
    var ele;
    var paperOffset = App.activeElement.offset();
    if (!it) {
        ele = page.set();
    //    ele.draggable.enable();
        var svg = $('svg#test');
        console.log(svg[0]);
        var cset = App.active_page.importSVG(svg[0]);
        var ft = App.active_page.freeTransform(cset).setOpts({"drag":['center'],"draw":['bbox']});
        ft.showHandles();

        var ox = 0;
        var oy = 0;
        var osx =0;
        var osy = 0;
        var dragging = false;




    }else{
        ele = page.path(it.path);
       // ele.draggable.enable();
        ele.attr(it.attr);
        page.canvas.id = it.id;
        ele.pathData = it.path;
        ele.it = it;
    }





    var centerX = (item.width() - ele.getBBox().width )/2;
    var centerY = (item.height() - ele.getBBox().height)/2;
    var tx = (-1 * ele.getBBox().x) + centerX
    var ty = (-1 * ele.getBBox().y) + centerY;
    var translate = 't' + tx + ',' + ty;
    ele.transform(translate);
    ele.centerX = centerX;
    ele.centerY = centerY;
    ele.item = item;
    ele.typeVal = type;
    ele.item_indx = idx;
    ele.item_height = item.height();
    ele.item_width = item.width();
    ele.ody = 0;
    ele.odx = 0;
    //enableItemControls(ele);

    decorateItem(ele);



}

function decorateItem (item){
    //move this


    var start = function () {

        this.odx = 0;
        this.ody = 0;
        this.animate({"fill-opacity": 0.2}, 500);

    };
    var move = function (dx, dy) {
       //set paper to full size
        this.paper.setSize($(window).width(), $(window).height());

        var idx = this.item_indx;
        var item_height = this.item_height;
        //offset the paper by the item height * by the index
        if (idx > 0 && !this.adj) {
            this.adj = true;
            this.paper.canvas.style.top = "-" + (item_height * idx);
            this.ody = this.ody - (item_height * idx);
        }
        this.translate(dx - this.odx, dy - this.ody);
        this.odx = dx;
        this.ody = dy;
        this.paper.canvas.style.zIndex = 100000;
        var paperOffset = $("#" + this.paper.canvas.id).offset();

        if (paperOffset) {
            var elementLeftOffset = paperOffset.left + this.getBBox().x + this.getBBox().width;
            var topOffset = paperOffset.top + this.getBBox().y;
            var pageLeft = App.activeElement.offset().left;
            if (elementLeftOffset >= pageLeft) {
                this.it.pleft = pageLeft;
                this.it.ptop = topOffset;
                this.enableDrop = true;
            }
        }

    };


    var up = function (e) {
        if(this.enableDrop){
//            var item = App.active_page.path(this.it.path);
//            item.attr(this.it.attr);






//            App.active_item = item;
//            //get x pos of where moved to
//            var boxX = item.getBBox().x;
//            var boxY = item.getBBox().y;
//            var xpos = ((this.odx - boxX) - this.it.pleft) + (item.getBBox().width / 2 ) + this.centerX - 20;
//            var ypos =((this.ody - boxY) + (this.item_indx * $('.item_id').height()) + $('.item_switch').height()) + this.centerY;
//
//            var transform = 't' +xpos+','+ ypos;
//            item.transform(transform);
//            item.animate({"fill-opacity": 1}, 500);
//
//
//            if (this.item_indx > 0) {
//                this.paper.canvas.style.top = "0px";
//            }
//
//
//
//            enableItemControls(item);
//
//
//            item.click(function (){
//                enableItemControls(item);
//            })
//
//            item.drag(move, start, function () {
//                this.animate({"fill-opacity": 1}, 500);
//            });
//            item.dblclick(function () {
//                this.remove();
//            });
        }else{

        }
        $('a#'+this.typeVal).trigger('click');
        var returnto = this.paper.returnTo;
        this.paper.setSize(returnto.width, returnto.height);


    };

    item.dblclick(function () {
        var ait = App.active_page.path(it.path);
        ait.attr(it.attr);
        ait.drag(move, start, up);
        ait.dblclick(function () {
            this.remove();
        });

    });
    item.mousedown(function (ele){
        this.mposx = this.clientX;
    });
    item.mouseup(function (ele) {

    });
    item.drag(move, start, up);
}

function enableItemControls(ele){
    var eleBox = ele.getBBox();

    var paper = ele.paper;

    paper.forEach(function (e){
           var ft = paper.freeTransform(e);
           ft.unplug();
    });
    var ft = paper.freeTransform(ele).setOpts({"drag":['center'],"draw":['bbox']});
   // ft.hideHandles();

    $('.item_controls > .actionbtn').each(function(){
        var raphFunc = $(this).data("function");

        if(ele[raphFunc]){

            $(this).unbind().bind("click",function(){
               if(raphFunc == "remove"){
                   paper.freeTransform(ele).unplug();
               }
               ele[raphFunc]();
            });
        }
    });
    $('.item_controls').show();
    return ft;
}
