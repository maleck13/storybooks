var items = {};

var _window = $(window);
var itemH = 150;
var page;
var menuItem;
var dbStore;
var App ={};
$('document').ready(function (){
  async.series([function(callback){
      Lawnchair({"name":"svg"},function (store){
          dbStore = store;
          callback();
      });
  }, function (callback){
      page = SVG('page1');
      var flip = $('#flipbook');
      var height = ($(window).height()/100) * 70;
      var width = ($(window).width()/100) *  70;
      flip.css("height",height+"px");
      flip.css("width",width+"px");
      flip.turn({
          height:height,
          autoCenter: true,
          gradients: !$.isTouch,
          elevation:50,
          display:'single'
      });
      var pages = {};

      flip.bind("turned", function(event, page, view) {

          var apage = $('#page' + page);
          if(!pages["page" + page]){
              // App.active_page = Raphael(document.getElementById('page' + page),apage.width(),apage.height());
              pages["page"+page] = App.active_page;
              App.activeElement = apage;

          }else{
              App.active_page = pages["page"+page];
          }
      });


      $('.item_choice').unbind().click(item_button);
      callback();
  }],function done(){
      console.log("done setting up");
  });

});

function item_button(e) {
    menuItem = $(this).attr("id");
    var item = $(this).data("item");
    var choices = $(this).data("choices").split(",");

    $('.item_choice').each(function (idx, b) {
        b = $(b);
        if (b.data("item") !== item) {
            b.removeClass("btn-primary");
            b.addClass("btn-info");
        } else {
            b.removeClass('btn-info');
            b.addClass("btn-primary");
        }
    });
    $('.item_id').remove();
    $('.item_switch').each(function (idx,ele){
        if(choices[idx]){
            $(this).html(choices[idx]);
            $(this).data("choice",choices[idx]);
            $(this).data("item",item);
            $(this).click(function (){
                getItems(item,$(this).data("choice"));
                $('.item_switch').each(function (idx,ele){
                   $(ele).removeClass("btn-inverse");
                });
                $(this).addClass("btn-inverse");
            });
        }
    });
    $('#item_switch').show();

    var numElements = Math.floor($(window).height() / 150);
    for (var i = 0; i < numElements; i++) {
        $('.item_list').append("<div class='span12 item_id' id=\"" + i + "\" style='z-index:1000000; height: "+itemH+"px; padding: 0px; margin: 0px; background-color: blueviolet; border-bottom: 1px solid blanchedalmond;'></div>")
    }



    getItems(item,"general");


}


function getItems(item,choice){
    var cache;
    async.waterfall([function (callback){
        dbStore.get(item, function(data){
            cache = data;
            var now = new Date().getTime();
           if(! data || now > data.value.expires) callback(undefined,undefined);
           else callback(undefined, data.value);
        });
    }, function (data,callback){
        if(data){
            callback(undefined, data);
        }else{
            //todo pull auth from cookie
            console.log("ajax call");
            $.ajax({
                "headers":{
                    "X-API_AUTH":"f1191d0a040dd7d5458d66bce63a841b",
                    "X-API_USER":"test@test.com"
                },
                "dataType":"json",
                "url":"http://localhost:8080/storybooks/svg/"+item+"/"+choice
            }).done(function (data){
                console.log("done",data);
                if(data.code === 200){
                    callback(undefined,data);
                }
            }).fail(function (data, stat){
                if(data.status === 0){
                    //no connection
                    if(cache){
                        populateItems(item,cache.value);
                        callback(undefined,undefined);
                    }
                }else{
                callback(data.status);
               }
            });
        }
    }, function (data,callback){
        if(data){
        data.expires = new Date().getTime() + 60000; // 1 min at mo
        dbStore.save({key:item,value:data},function (data){
            populateItems(item,data.value);
            callback();
        });
        }else callback(undefined,undefined);
    }], function done(err, oks){

        console.log("done waterfall",err,oks);
    });
}

function populateItems(item,data){
        items[item] = data.result[0];
        $('.item_id').each(function (idx, it){
            addItem(idx,it,item);
        });
}


var els = [];
function addItem (idx, e, type) {
    var item = $(e);

    var it = items[type][idx];
    if(! it) return;
    var page = SVG(document.getElementById(item.attr("id"))).size(item.width(), item.height());
    page.returnTo = {"height": item.height(), "width": item.width()};
    var ele = page.group();

      page.svg(it.svg, function (){
          ele.add(this);
      });
      ele.svg = it.svg;


    els[idx] = ele;
    ele.move(15 - ele.bbox().x, 15 - ele.bbox().y);
    ele.draggable();
    ele.beforedrag = function (e){



        this.parent.size(_window.width(),_window.height());

        var _this = $("#"+this.parent.node.id);
        _this.css("z-index",1000000);
        var offTop = (_this.offset().top);

        var top = -1 * offTop ;
        _this.css("top",top + "px");
        console.log(typeof this.transform);
        var ypos = -1 * this.trans.y;
        ypos = (e.pageY - ypos) - (this.bbox().height /2 );
        this.transform({y:ypos});


    };
    ele.dragstart = down;
    ele.dragend = end;

    ele.idx = idx;
}

function down(e){
    this.opacity(0.5);
};
var id = 1
function end(e,event){
    var group = page.group();



    group.id= id;
    var store = page.svg(this.svg, function (){

        if(this instanceof  SVG.Path){
            this.data('import-id', 'element-' + id)
            id++
            group.add(this);
        }
    });
    var gbox = group.bbox();
    var crect = page.rect(group.bbox().width,group.bbox().height);
    crect.citem = true;

    crect.on("mousemove", function (e){
       console.log(gbox);

    });

    crect.attr({x:gbox.x,y:gbox.y,fill:"#b1c9ed",opacity:0.3,stroke:'#2f7cef','stroke-width':2,'stroke-dasharray':[3]});
    var contrlHandle= group.line(gbox.cx,gbox.cy, gbox.cx + (gbox.width / 1.4), gbox.cy);
    contrlHandle.attr({"stroke-width":2,"stroke":"#2f7cef"});
    contrlHandle.citem = true;
    var cntrlCircle = group.circle(20);
    cntrlCircle.attr({"stroke":"#2f7cef","fill":"#b1c9ed","cx":gbox.cx + (gbox.width / 1.4), "cy":gbox.cy});
    cntrlCircle.citem = true;
    var mouseout = false;
    cntrlCircle.on("mousedown",function (e){
        var y = e.pageY;
        mouseout = false;
        cntrlCircle.on("mousemove",function(e){
            if(mouseout) return;
            if(e.pageY < y || e.pageY > y){
                console.log("diff", e.pageY - y);
                group.rotate(e.pageY -y);
            }
        });
    });

    cntrlCircle.on("mouseout", function (e){
        mouseout = true;
    });

    cntrlCircle.on("mouseup",function(){

    });
    group.add(crect,0);


    group.store = store;







    resizeMenuBox.call(this);
    transformToPagxPagy(group,event.pageX,event.pageY);
    this.opacity(1);
    applyPageCtrls(group);

    $('.item_choice#'+menuItem).trigger("click");


}


function applyPageCtrls(ele){



    ele.forward = function (){
        var pos = ele.position();
        ele.remove();
        page.add(ele,pos + 2);
    };
    ele.backward = function (){
      var pos = ele.position();
      ele.remove();
      page.add(ele,pos -2);
    };

    ele.draggable();

    ele.dragstart = function (){
        this.opacity(0.5);
    };
    ele.dragend = function (){
        this.opacity(1);
    };

    ele.on("click",function(){
       addCtrlBox(ele);
    });



    addCtrlBox(ele);
}


function addCtrlBox(ele){
    console.log(page._children);
    for(var i=0; i < page._children.length; i++){
        if(page._children[i] instanceof  SVG.G){
            console.log("looking at group" , ele.id , page._children[i].id);
            if(page._children[i].id !== ele.id){

                page._children[i].each(function (){
                    if(this.citem){
                        this.hide();
                    }
                });
            }else{
                page._children[i].each(function (){
                    if(this.citem){
                        this.show();
                    }
                });
            }
        }
    }
    page.each(function(idx,eles){
       console.log("each ", idx);
    });

    $('.item_controls > .actionbtn').each(function(){
    var ctlFunc = $(this).data("function");

        if(ele[ctlFunc]){
            $(this).unbind().bind("click",function(){

                ele[ctlFunc].call(ele);
            });
        }
    });
    $('.item_controls').show();
}

function resizeMenuBox(){
    var _this = $("#"+this.parent.node.id);
    _this.css("z-index",1000000);
    var offTop = (_this.offset().top);
    var top = -1 * offTop ;
    _this.css("top","0px");
    var returnTo = this.parent.returnTo;
    this.parent.size(returnTo.width,returnTo.height);
}

function transformToPagxPagy(ele,pageX,pageY){

    var jpage = $('#'+ page.node.id);
    var pposx = pageX - jpage.offset().left;
    var pposy = pageY - jpage.offset().top;
    var bbox  = ele.bbox();
    if(navigator.userAgent.match(/Firefox/)){
        pposx-= 100;
        pposy-=80;
    };

    var ty = (pposy - bbox.y) - (bbox.height / 2);
    var tx = (pposx - bbox.x)- (bbox.width / 2.5);


    ele.transform({y:ty,x:tx});
}


