$('document').ready(function () {
    $('.item_choice').unbind().click(item_button);
    $('.item_choice').first().trigger("click");
});


var items = {
    "sky": [
        {"name": "fluffy cloud",
            "id": "fc",
            "path": 'M 151.6 120.7 C 151.3 120.7 151 120.7 150.7 120.7 C 145.7 121.2 141.2 124.4 139 129.2 L 137.8 132.8 C 138.1 131.5 138.4 130.3 139 129.2 C 135 125 129.3 123.1 123.8 124 C 118.2 124.9 113.3 128.5 110.7 133.8 C 103.2 129.2 93.9 129.4 86.7 134.5 C 79.5 139.5 75.7 148.5 76.9 157.5 L 77.6 161.1 C 77.3 159.9 77 158.7 76.9 157.5 L 76.7 157.8 C 70.5 158.5 65.3 163.3 64 169.8 C 62.7 176.3 65.4 183 70.9 186.4 L 79.4 188.4 C 76.4 188.7 73.4 188 70.9 186.4 C 66.7 191.1 65.7 198 68.5 203.8 C 71.3 209.5 77.2 212.8 83.2 212 L 86.9 211.1 C 85.7 211.6 84.5 211.9 83.2 212 C 86.7 218.4 92.4 223 99.2 224.9 C 105.9 226.7 113 225.6 119 221.9 C 123.8 229.5 132.5 233.6 141.1 232.2 C 149.8 230.8 156.9 224.3 159.4 215.5 L 160.3 210.6 C 160.2 212.3 159.9 213.9 159.4 215.5 C 165.4 219.4 172.9 219.6 179 216.1 C 185.2 212.5 189 205.8 189 198.4 L 186.8 187.4 L 178.2 180 C 184.9 184.2 189.1 190.6 189 198.4 C 197 198.5 203.8 191.7 206.9 183.8 C 209.9 176 208.7 167 203.8 160.3 C 205.9 155.3 205.7 149.6 203.5 144.7 C 201.3 139.8 197.1 136.1 192.1 134.7 C 191 128.2 186.4 123 180.3 121.3 C 174.2 119.6 167.8 121.7 163.7 126.7 L 161.2 130.9 C 161.9 129.4 162.7 128 163.7 126.7 C 160.8 122.8 156.3 120.5 151.6 120.7 L 151.6 120.7Z',
            "attr": {'stroke-width': '5', 'stroke': '#e0d5d5', 'fill': '#fcf9f9', 'stroke-opacity': '1'}
        },
        {"name": "bright sun",
            "id": "bs",
            "path": 'M 305 211.2 L 283.4 218.5 L 283.4 203.9 L 305 211.2 Z M 289.3 175.1 L 279.5 194.9 L 268.6 184.5 L 289.3 175.1 Z M 251.5 160.2 L 259.2 180.8 L 243.8 180.8 L 251.5 160.2 Z M 213.7 175.1 L 234.4 184.5 L 223.5 194.9 L 213.7 175.1 Z M 198 211.2 L 219.6 203.9 L 219.6 218.5 L 198 211.2 Z M 213.7 247.3 L 223.5 227.5 L 234.4 237.9 L 213.7 247.3 Z M 251.5 262.2 L 243.8 241.6 L 259.2 241.6 L 251.5 262.2 Z M 289.3 247.3 L 268.6 237.9 L 279.5 227.5 L 289.3 247.3 Z M 224.8 211.2 L 224.8 211.2 C 224.8 197.1 236.7 185.7 251.5 185.7 C 266.3 185.7 278.3 197.1 278.3 211.2 C 278.3 225.3 266.3 236.7 251.5 236.7 C 236.7 236.7 224.8 225.3 224.8 211.2Z',
            "attr": {'fill': '#efef1f', 'stroke-width': '5', 'stroke': '#000000', 'stroke-opacity': '1'}
        },
        {
            "name": "sparkly sun",
            "id": "ss",
            "path": 'M 156.2 172.4 L 170.9 168.5 L 157.3 162.1 L 172.6 160.9 L 160.6 152.1 L 175.9 153.7 L 166 143 L 180.6 147.2 L 173.2 134.9 L 186.6 141.7 L 182 128.3 L 193.7 137.3 L 192 123.4 L 201.6 134.4 L 202.9 120.4 L 210 132.8 L 214.2 119.4 L 218.5 132.8 L 225.5 120.4 L 226.8 134.4 L 236.4 123.4 L 234.7 137.3 L 246.4 128.3 L 241.8 141.7 L 255.2 134.9 L 247.8 147.2 L 262.4 143 L 252.6 153.7 L 267.8 152.1 L 255.8 160.9 L 271.1 162.1 L 257.5 168.5 L 272.2 172.4 L 257.5 176.3 L 271.1 182.7 L 255.8 183.9 L 267.8 192.7 L 252.6 191.1 L 262.4 201.9 L 247.8 197.6 L 255.2 209.9 L 241.8 203.1 L 246.4 216.5 L 234.7 207.5 L 236.4 221.4 L 226.8 210.4 L 225.5 224.4 L 218.5 212 L 214.2 225.4 L 210 212 L 202.9 224.4 L 201.6 210.4 L 192 221.4 L 193.7 207.5 L 182 216.5 L 186.6 203.1 L 173.2 209.9 L 180.6 197.6 L 166 201.9 L 175.9 191.1 L 160.6 192.7 L 172.6 183.9 L 157.3 182.7 L 170.9 176.3 L 156.2 172.4Z',
            "attr": {'stroke': '#ffff00', 'stroke-linecap': 'null', 'stroke-linejoin': 'null', 'stroke-width': '5', 'fill': '#efef1f', 'stroke-opacity': '1'}
        },
        {
            "name":"dark cloud",
            "path":"m92.42116,4.52637c-13.04422,0 -24.24002,6.76514 -29.72084,16.52893c-2.18933,-1.21991 -4.60345,-1.95872 -7.18018,-1.95872c-7.79234,0 -14.27504,6.26709 -16.27997,14.77536c-3.4205,-1.35118 -7.39809,-2.17088 -11.64597,-2.17088c-12.74573,0 -23.08421,6.97266 -23.08421,15.56966c0,7.7612 8.4794,14.14029 19.49412,15.32193c-0.4939,0.83607 -0.87676,1.66615 -0.87676,2.54543c0,8.23083 22.55045,14.94411 50.34205,14.94411c27.79208,0 50.3432,-6.71327 50.3432,-14.94411c0,-0.55322 -0.34396,-1.04797 -0.54248,-1.58752c1.21692,0.30359 2.42279,0.62756 3.7561,0.62756c7.4357,0 13.48378,-5.05104 13.48378,-11.27063c0,-6.21994 -6.04808,-11.27217 -13.48378,-11.27217c-0.91078,0 -1.68483,0.31514 -2.54604,0.4604c0.7533,-2.48816 1.25197,-5.02457 1.25197,-7.72356c0,-16.46077 -14.90048,-29.84578 -33.311,-29.84578l0,0z",
            "attr":{"stroke-linecap":"null", "stroke-linejoin":"null", "stroke-dasharray":"null", "stroke-width":"5", "stroke":"#efefe8", "fill":"#ccccc5"}
        }
    ]
};

App.items = items;


//var start = function () {
//    this.odx = 0;
//    this.ody = 0;
//    this.animate({"fill-opacity": 0.2}, 500);
//};
//var move = function (dx, dy) {
//
//
//    this.paper.setSize($(window).width(), $(window).height());
//
//    var idx = this.item_indx;
//    var item_height = this.item_height;
//    var item_width = this.item_width;
//    if (idx > 0 && !this.adj) {
//        console.log("making adjustments setting top to ", item_height * idx, idx);
//        this.adj = true;
//        this.paper.canvas.style.top = "-" + (item_height * idx);
//        this.ody = this.ody - (item_height * idx);
//    }
//    this.translate(dx - this.odx, dy - this.ody);
//    this.odx = dx;
//    this.ody = dy;
//    this.paper.canvas.style.zIndex = 100000;
//    var paperOffset = $("#" + this.paper.canvas.id).offset();
//
//    if (paperOffset) {
//        var elementLeftOffset = paperOffset.left + this.getBBox().x + this.getBBox().width;
//        var topOffset = paperOffset.top + this.getBBox().y;
//        var pageLeft = App.activeElement.offset().left;
//        var pageTop = App.activeElement.offset().top;
//        if (elementLeftOffset >= pageLeft) {
//            var lOffset = ( elementLeftOffset  - pageLeft);
//            console.log("offset left ", lOffset, pageLeft, elementLeftOffset, this.getBBox().x);
//            var x = this.getBBox().x;
//            var y = this.ody;
//
//            console.log("add to page  diff" + x, y);
//            this.it.pleft = pageLeft;
//            this.it.ptop = topOffset;
//
//        }
//    }
//
//};
//
//function getzzTrans(item) {
//    var translate = 't' + ((-1 * item.getBBox().x)+item.getBBox().width/2) + ',' + ((-1 * item.getBBox().y) + item.getBBox().y /4);
//    console.log("trans",translate);
//    return translate;
//}
//
//var up = function (e) {
//    var item = App.active_page.path(this.it.path);
//    item.attr(this.it.attr);
//    App.active_item = item;
//    //get x pos of where moved to
//    console.log("box x ", item.getBBox().x, "offset " + this.odx);
//    var boxX = item.getBBox().x;
//    var boxY = item.getBBox().y;
//    var xpos = ((this.odx - boxX) - this.it.pleft) + (item.getBBox().width /2 );
//    var ypos =((this.ody - boxY) + (this.item_indx * $('.item_id').height()));
//
//    var transform = 't' +xpos+','+ ypos;
//    item.transform(transform);
////        console.log("transform string ", transform);
////
//    this.animate({"fill-opacity": 1}, 500);
//    var returnto = this.paper.returnTo;
//    this.paper.setSize(returnto.width, returnto.height);
//    if (this.item_indx > 0) {
//        this.paper.canvas.style.top = "0px";
//    }
//    this.paper.clear();
//    var nitem = this.paper.path(this.it.path);
//    nitem.attr(this.it.attr);
//    nitem.transform(getzzTrans(nitem));
//    nitem.animate({"fill-opacity": 1}, 500);
//
//
//    item.drag(move, start, function () {
//        this.animate({"fill-opacity": 1}, 500);
//    });
//    item.dblclick(function () {
//        this.remove();
//    });
//};



//
//function drawPath(path, attr, paper) {
//
//    var Obj = paper.path(path);
//    Obj.attr(attr);
//
//    //Obj.translate(-1*Obj.getBBox().x,-1*Obj.getBBox().y);
//
//    var translate = 't' + ((-1 * Obj.getBBox().x) + 15) + ',' + ((-1 * Obj.getBBox().y) + 15);
//    Obj.transform(translate);
//    Obj.getBBox().x = 15;
//    Obj.getBBox().y = 15;
//    return Obj;
//}
