/**
 * Created with JetBrains WebStorm.
 * User: craigbrookes
 * Date: 19/04/2013
 * Time: 21:39
 * To change this template use File | Settings | File Templates.
 */
$('document').ready(function (){
    var page = SVG('canvas');
    console.log(page);
    var flip = $('#flipbook');
    var height = ($(window).height()/100) * 70;
    var width = ($(window).width()/100) *  70;
    flip.css("height",height+"px");
    flip.css("width",width+"px");
    flip.turn({
        height:height,
        autoCenter: true,
        gradients: !$.isTouch,
        elevation:50,
        display:'single'
    });
    var pages = {};
    var App ={};
    flip.bind("turned", function(event, page, view) {
        console.log(event,page, view);
        var apage = $('#page' + page);
        if(!pages["page" + page]){
            // App.active_page = Raphael(document.getElementById('page' + page),apage.width(),apage.height());
            pages["page"+page] = App.active_page;
            App.activeElement = apage;

        }else{
            App.active_page = pages["page"+page];
        }
    });

    page.rect(200,200);

});