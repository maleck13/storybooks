window.App = {};
App.host = "http://localhost:8080/";
//App.host = "http://api.storybooks.me/";
App.storybook = {
    "element": "",
    "width": 0,
    "height": 0,
    "pages": {},
    "activePage": "",
    "activeElement": ""
};
App.storybook.userContent = {
    "title": "",
    "content": "",
    "id": undefined,
    "userId": undefined
};
App.db = {
    "name": "storybook",
    "conn": undefined
};
App.menu = {
    "defaultItem": "",
    "defaultHeight": 150
};

App.items = [];

var headers = {
    "X-API-AUTH": "8d63c7b02c6b46ec0d56a77f9687bfd1",
    "X-API-USER": "test@test.com"
};

var item_id = 0;

var _window = $(window);

$('document').ready(function () {

    App.menu.defaultItem = $('ul > li .default').attr("id");
    async.parallel(App.builder.init.parallel,
        function doneSetup(err, oks) {
            console.log("done set up", err);

        });
    initPersistance(function () {
        async.series(App.builder.init.series, function (err, oks) {
            console.log("finished series setup", oks);
            $('a#sky').trigger("click");
        });
    });

//    var sockjs_url = 'http://localhost:9000/colab';
//    var sockjs = new SockJS(sockjs_url,null,{"debug":true,"devel":true,"protocols_whitelist":["websocket"]});
//    sockjs.onopen    = function()  {console.log('[*] open', sockjs.protocol);};
//    sockjs.onmessage = function(e) {console.log('[.] message', e);};
//    sockjs.onclose   = function()  {console.log('[*] close');};
//
//    $('#colab').click(function (){
//        sockjs.send(JSON.stringify({"channel":"test"}));
//    });


});

//db data layer
function initPersistance(cb) {
    if (App.db.conn)return cb();

    Lawnchair({"name": App.db.name}, function (store) {
        App.db.conn = store;
        console.log("finished setting up lawnchair using " + this.adapter);
        cb();
    });
}

(function () {

    var checkLocalDb = function (itemkey, callback) {
        App.db.conn.get(itemkey, function (data) {
            console.log("retrieved data for key " + itemkey + " from cache", data);
            var now = new Date().getTime();
            if (!data || now > data.value.expires) callback(undefined, undefined);
            else callback(undefined, data.value);
        });
    };

    var apiCall = function () {
    };

    App.db.getItems = function (item, choice, cb) {
        var key = item + choice || "";
        async.waterfall([
        function (callback) {
            checkLocalDb(key, callback);
        },
        function (data, callback) {
            if (data) {
                callback(undefined, data);
            } else {

                $.ajax({
                    "headers": headers,
                    "dataType": "json",
                    "url": App.host + "/svg/" + item + "/" + choice
                }).done(function (data) {
                        if (data.code === 200) {
                            callback(undefined, data);
                        }
                    })
                    .fail(function (data, stat) {
                        if (data.status === 0) {
                            //no connection
                            if (cache) {
                                callback(undefined, data.value);
                            }
                        } else {
                            callback(data.status);
                        }
                    });
            }
        }, function (data, callback) {
            if (data) {
                data.expires = (data.expires) ? data.expires : data.expires = new Date().getTime() + 6000; // 1 min at mo
                App.db.conn.save({key: key, value: data}, function (data) {

                    callback(undefined, data.value);
                });
            } else callback(undefined, undefined);
        }], function done(err, data) {

            console.log("done waterfall", err, data);
            cb(undefined, data);
        });
    };

    App.db.loadBook = function (bookId, cb) {
        async.waterfall([function (callback) {
            checkLocalDb(bookId, callback);
        }, function (data, callback) {
            console.log("loaded data from localdb ", data);
            if (data)callback(undefined, data);
            else {
                $.ajax({
                    "headers": headers,
                    "dataType": "json",
                    "url": App.host + "/storybook/" + bookId
                }).done(function (data) {
                        console.log("done", data);
                        if (data.code === 200) {
                            callback(undefined, data.result[0]);
                        }
                    }).fail(function (data, stat) {
                        if (data.status === 0) {
                            //no connection
                            if (cache) {

                                callback(undefined, data.value);
                            }
                        } else {
                            callback(data.status);
                        }
                    });
            }
        }], cb);
    };

    App.db.listBooks = function (cb) {
        $.ajax({
            "headers": headers,
            "dataType": "json",
            "url": App.host + "/storybook/list"
        }).done(function (data) {
                if (data.code === 200 && data.result && data.result[0].length > 0) {
                    var bl = $('#booklist');
                    var table = "<table class='table'></table>";
                    table = $(table);

                    data.result[0].forEach(function (res) {
                        table.append("<tr><td style='width: 75%;'>" + res.title + "</td><td><button data-id='" + res.id + "' class='btn load'>Load</button></td></tr>");
                    });
                    bl.empty();
                    bl.append(table);
                    $(table).find('button.load').unbind().bind("click", function () {
                        var id = $(this).data("id");
                        console.log("loading book" + id);
                        App.db.loadBook(id, function (err, data) {
                            console.log("retrieved data ", err, data);
                            var book = data.content;
                            var itemid = 0;
                            for (var page in book) {
                                console.log("page in book ", book);
                                if (book.hasOwnProperty(page) && App.storybook.pages[page]) {
                                    console.log("has page");
                                    App.storybook.pages[page].clear();
                                    App.storybook.pages[page].fromJSON(book[page], function (el) {
                                        App.items = App.items || {};
                                        el.itemid = itemid;
                                        App.items[itemid] = el;
                                        itemid++;

                                        el.click(function () {
                                            addItemControls(el);
                                        });
                                        return el;
                                    });
                                } else if (book.hasOwnProperty(page)) {
                                    console.log("has page book");
                                    var apage = $('#' + page);
                                    if (!App.storybook.pages[page]) {
                                        App.storybook.pages[page] = Raphael(apage[0], apage.width(), apage.height());
                                        App.storybook.pages[page].fromJSON(book[page], function (el) {
                                            App.items = App.items || {};
                                            el.itemid = itemid;
                                            App.items[itemid] = el;
                                            itemid++;

                                            el.click(function () {
                                                console.log("clicked after load");
                                                addItemControls(el);

                                            });
                                            return el;
                                        });
                                    }
                                }
                                var width = App.storybook.pages["page1"].width;
                                var height = App.storybook.pages["page1"].height;
                                //needs centralising
                                App.storybook.pages["page1"].forEach(function (ele) {

                                    if (ele.type === "text") {
                                        App.storybook.pages["page1"].inlineTextEditing(ele);
                                        textControls(ele);
                                    }
                                });
                                $('#books').modal('hide');
                            }
                        });
                    });
                    $('#books').modal();
                    cb();
                }

            }).fail(function (data, stat) {
                if (data.status === 0) {
                    //no connection

                } else {

                }
                cb();
            });
    };

    App.db.deleteBook = function (bookId, cb) {

    };
})();


//builder controller;
(function () {

    App.builder = {
        initFlipBook: function (cb) {
            console.log("setting up flipbook");
            App.storybook.element = $('div#flipbook');
            var height = ($(window).height() / 100) * 70;
            var width = ($(window).width() / 100) * 70;
            App.storybook.element.height = height;
            App.storybook.element.width = width;
            App.storybook.element.css("height", height + "px");
            App.storybook.element.css("width", width + "px");
            App.storybook.flip = $('div#flipbook').turn({
                height: height,
                autoCenter: true,
                gradients: !$.isTouch,
                elevation: 50,
                display: 'single'
            });
            var page1 = $('#page1');
            App.storybook.element.bind("turned", function (event, page, view) {

                var apage = $('#page' + page);
                if (!App.storybook.pages["page" + page]) {
                    App.storybook.activePage = Raphael(document.getElementById('page' + page), apage.width(), apage.height());
                    App.storybook.pages["page" + page] = App.storybook.activePage;
                    App.storybook.pages["page" + page].activeElement = apage;
                } else {
                    App.storybook.activePage = App.storybook.pages["page" + page];
                }
            });

            App.storybook.activePage = Raphael(document.getElementById('page1'), page1.width(), page1.height());
            App.storybook.pages["page1"] = App.storybook.activePage;

            App.storybook.activePage.activeElement = page1;

            console.log("finished setting up flipbook ", App.storybook);
            cb();
        },


        initData: function (cb) {
            var item = App.menu.defaultItem;
            App.db.getItems(item, "general", cb);
        },

        drawMenuItem: function (ele, svg, index, cb) {
            var item = $(ele);
            var page = Raphael(document.getElementById(item.attr("id")), item.width(), item.height());
            page.returnTo = {"height": item.height(), "width": item.width()};
            var g = page.importSVG(svg);
            g.index = index;
            g.svg = ele.svg;
            g.group = ele.group;
            g.item_height = item.height();
            g.item_width = item.width();
            g.ody = 0;
            g.odx = 0;
            var centerX = (item.width() - g.getBBox().width ) / 2;
            var centerY = (item.height() - g.getBBox().height) / 2;
            var tx = (-1 * g.getBBox().x) + centerX
            var ty = (-1 * g.getBBox().y) + centerY;
            var translate = 't' + tx + ',' + ty;
            g.transform(translate);
            itemDecorator(g);

        },
        addSvg: function () {

        },
        initMenu: function (cb) {
            var fullScreenChoice = $('.full_screen_choice');
            var fullScreenChoiceTemp = "<div style='height:300px;' class='span4 choice' ></div>";
            fullScreenChoice.unbind().click(function () {
                var ele = $(this);
                var menuItem = ele.attr("id");
                var item = ele.data("item");
                var choices = ele.data("choices").split(",");
                console.log("fullScreenChoice");
                App.db.getItems(item, "general", function (err, data) {
                    if (err) {
                        console.log("err ", err);
                    }

                    var items = data["result"][0];
                    $('#mainpanel').hide();
                    var gridpanel = $('#gridpanel');
                    gridpanel.empty();
                    gridpanel.show();

                    items.forEach(function (ele, idx) {
                        gridpanel.append(fullScreenChoiceTemp);
                    });
                    var tmp = $('#tmpsvg');
                    $('.choice').each(function (idx) {
                        var tmpPage = Raphael($(this)[0]);

                        tmp.append(items[idx].svg);
                        var svg = tmp.children('svg');
                        var w = svg.attr("width");
                        var h = svg.attr("height");
                        var sw = tmpPage.width / w;
                        var sh = tmpPage.height / h;
                        var ng = tmpPage.importSVG(tmp[0]);
                        var ft = tmpPage.freeTransform(ng).hideHandles();
                        ft.attrs.scale.x = sw;
                        ft.attrs.scale.y = sh;
                        ft.apply();
                        ft.attrs.translate.x = -1 * ng.getBBox().x;
                        ft.attrs.translate.y = -1 * ng.getBBox().y;
                        ft.apply();
                        //console.log(ft);
                        tmp.empty();
                        ng.click(function () {
                            gridpanel.empty();
                            gridpanel.hide();
                            tmp.append(items[idx].svg);
                            var bg = App.storybook.activePage.importSVG(tmp[0]);
                            var ftbg = tmpPage.freeTransform(bg).hideHandles();

                            var sw = App.storybook.activePage.width / w;
                            var sh = App.storybook.activePage.height / h;
                            ftbg.attrs.scale.x = sw;
                            ftbg.attrs.scale.y = sh;
                            ftbg.apply();
                            ftbg.attrs.translate.x = -1 * bg.getBBox().x;
                            ftbg.attrs.translate.y = -1 * bg.getBBox().y;
                            ftbg.apply();
                            console.log("before", bg);

                            var btm = App.storybook.activePage.getById(0);
                            console.log("bottom ", btm);
                            if (btm != null) {
                                bg.insertBefore(btm);
                            }
                            console.log("after", bg);
                            tmp.empty();

                            $('#mainpanel').show();
                        });
                    });

                });


                return false;
            });


            var item_choice = $('.item_choice');
            item_choice.unbind().click(function () {
                $('#gridpanel').hide();
                $('#mainpanel').show();
                var ele = $(this);
                var menuItem = ele.attr("id");
                var item = ele.data("item");
                var choices = ele.data("choices").split(",");
                var numElements = Math.floor($(window).height() / 150);
                item_choice.each(function (idx, b) {
                    b = $(b);
                    if (b.data("item") !== item) {
                        b.removeClass("btn-primary");
                        b.addClass("btn-info");
                    } else {
                        b.removeClass('btn-info');
                        b.addClass("btn-primary");
                    }
                });
                $('.item_id').remove();
                $('.item_switch').each(function (idx, ele) {

                    if (choices[idx]) {
                        var it = $(this);
                        it.html(choices[idx]);
                        it.data("choice", choices[idx]);
                        it.data("item", item);
                        it.unbind().bind("click", function () {
                            $('.item_id').remove();
                            for (var i = 0; i < numElements; i++) {
                                $('.item_list').append("<div class='span12 item_id' id=\"" + i + "\" style='z-index:1000000; height: " + App.menu.defaultHeight + "px; padding: 0px; margin: 0px; background-color: blueviolet; border-bottom: 1px solid blanchedalmond;'></div>")
                            }
                            //todo load data
                            console.log("calling for " + item + " " + choices[idx]);
                            App.db.getItems(item, choices[idx], function (err, data) {
                                populateItems(err, data, item);
                            });
                            $('.item_switch').each(function (idx, ele) {
                                $(ele).removeClass("btn-inverse");
                            });
                            $(this).addClass("btn-inverse");
                        });
                    }
                });
                $('#item_switch').show();

                var numElements = Math.floor($(window).height() / 150);
                for (var i = 0; i < numElements; i++) {
                    $('.item_list').append("<div class='span12 item_id' id=\"" + i + "\" style='z-index:1000000; height: " + App.menu.defaultHeight + "px; padding: 0px; margin: 0px; background-color: blueviolet; border-bottom: 1px solid blanchedalmond;'></div>")
                }
                App.db.getItems(item, "general", function (err, data) {
                    populateItems(err, data, item);
                });


            });
            cb();
        },
        "initBook": function (cb) {
            //load existing books and wait for user to choose one or else start with blank slate
            //abstract ajax to persist.js


            App.db.conn.get("book", function (data) {

                var bookdata = (data && data.value) ? data.value : "Add Your Book Title Here.";
                var width = App.storybook.pages["page1"].width;
                var height = App.storybook.pages["page1"].height;
                var title = App.storybook.pages["page1"]
                    .text((width / 2) - 100, (height / 2) - 20, bookdata.title || "add a title")
                    .attr({"font-size": "20px", "font-family": "arial black"});

                App.storybook.pages["page1"].inlineTextEditing(title);
                title.click(function () {
                    textControls(title);
                });
                for (var es in title.events) {
                    if (title.events[es].name === "click") {
                        title.events[es].f.call(title);
                    }
                }
                cb();
            });


        },
        "loadBook": function (cb) {
            $('#loadbutton').unbind().bind('click', function () {
                App.db.listBooks(cb);
            });
        },
        "saveBook": function (cb) {
            console.log("save book setup");
            $("#savebutton").click(function () {

                var toSave = {"content": {}};
                var len = App.items.length;

                removeFreeTrans();
                for (var jP in App.storybook.pages) {
                    var cPage = (App.storybook.pages.hasOwnProperty(jP)) ? App.storybook.pages[jP] : undefined;
                    if (cPage) {


                        toSave.content[jP] = cPage.toJSON();
                        toSave.title = App.storybook.userContent.title;
                        toSave.ownerId = headers["X-API-USER"];
                        console.log("toSave", toSave);
                    }
                }
                console.log(toSave);
                // App.storybook.activePage.freeTransform(activeItem).showHandles();
                $.ajax({
                    "headers": headers,
                    "dataType": "json",
                    "url": App.host + "/storybook",
                    "method": "POST",
                    "beforeSend": function () {
                    },
                    "data": JSON.stringify(toSave),
                    "contentType": "application/json "
                }).done(function (data) {
                        console.log("done", data);
                        if (data.code === 200) {
                            var bookid = data.result[0].id;
                            toSave.bookId = bookid;
                            App.db.conn.save({"key": bookid, "value": toSave}, function (ok) {
                                console.log("local save", ok);
                            });
                        }
                    }).fail(function (data, stat) {
                        if (data.status === 0) {
                            //no connection
                            console.log("no connection");
                        } else {
                            console.log(data);
                        }
                    });
            });
        },
        "initBindings": function (cb) {
            console.log("init bindings");
            async.parallel([App.builder.saveBook, App.builder.loadBook], cb);
        },


        init: {
            parallel: [],
            series: []
        }

    };

    App.builder.init.parallel = [App.builder.initFlipBook, App.builder.initBindings];
    App.builder.init.series = [App.builder.initData, App.builder.initMenu, App.builder.initBook];

})();


function itemDecorator(item) {
    //movement functions
    var width = _window.width();
    var height = _window.height();
    var canvas = $(item.paper.canvas);
    var offTop = canvas.offset().top;
    var offleft = canvas.offset().left;
    item.offtop = offTop;
    item.offleft = offleft;
    var ft = item.paper.freeTransform(item, { drag: "center", boundary: {"height": height, width: width} }, function (ft, event) {

        ft.attrs.translate.y = offTop;
        if (event.indexOf("drag start") != -1) {
            dragStart();
        }
        else if (event.indexOf("drag end") != -1) {
            console.log(ft);
            dragEnd();
        }
        else if (event[0] === "drag") {
            dragging();
        }

    });

    ft.showHandles();


    function dragStart() {
        item.animate({"fill-opacity": 0.2}, 500);
        item.returnWidth = item.paper.width;
        item.returnHeight = item.paper.height;
        item.paper.setSize(width, height);
        item.paper.canvas.style.zIndex = 100000;
        var canvas = $(item.paper.canvas);
        var offTop = canvas.offset().top;
        var top = -1 * offTop;
        canvas.css("top", top + "px");
        item.freeTransform.attrs.translate.y = offTop;
        item.freeTransform.apply();
    }

    function dragging() {
        // the left offset of the menu box + the x of the items boundarybox plus the width of the item
        var elementLeftOffset = offleft + item.getBBox().x + item.getBBox().width;
        var elementTopOffset = offTop + item.getBBox().y - item.getBBox().height;
        var can = $(App.storybook.activePage.canvas);

        var pageOffsetLeft = $(App.storybook.activePage.canvas).offset().left; // App.storybook.activePage.canvas.offsetLeft; //
        var pageOffsetTop = $(App.storybook.activePage.canvas).offset().top; // App.storybook.activePage.canvas.offsetTop; //
        // console.log("element top", elementTopOffset, "page top ", pageOffsetTop, "diff", elementTopOffset - pageOffsetTop);
        //  console.log("element left", elementLeftOffset, "page top ", pageOffsetLeft, "diff", elementLeftOffset - pageOffsetLeft);
        //elementLeftOffset - pageOffsetLeft gives x on the storybook page need to translate that from the x of the add item
        //on the page x -
        if (elementLeftOffset >= pageOffsetLeft) {
            //    console.log("should drop");
            item.shouldDrop = true;
            item.xpos = (elementLeftOffset - item.getBBox().width - pageOffsetLeft);
            item.ypos = (elementTopOffset - pageOffsetTop) - (150 * item.index) - (item.getBBox().height / 2);
        } else {
            item.shouldDrop = false;
        }
    }


    function dragEnd() {
        if (item.shouldDrop) {

            var tmp = $('#tmpsvg');
            tmp.append(item.svg);

            var ng = App.storybook.activePage.importSVG(tmp[0]);

            var ft = App.storybook.activePage.freeTransform(ng, {"drag": "center", "draw": "bbox"}).showHandles();

            tmp.empty();

            ft.attrs.translate.x = item.xpos - ng.getBBox().x;
            ft.attrs.translate.y = item.ypos - ng.getBBox().y;
            console.log("xpos", item.xpos);
            ft.apply();
            item.remove();
            ng.itemid = App.items.length;
            ng.active = true;

            ng.click(function () {
                addItemControls(ng);
            });
            if ("text" === item.group) {

                var activePage = App.storybook.activePage.activeElement;
                activePage.append("<textarea class='hide'  wrap='hard' style='font-size: 12pt' id='boxtext'></textarea>");
                var ngBbox = ng.getBBox();
                var percentWidth = (ngBbox.width / 100) * 85;
                var percentHeight = (ngBbox.height / 100) * 80;
                var leftoff = item.xpos + ( ngBbox.width / 100 ) * 5;
                var topoff = item.ypos + (ngBbox.height / 100) * 5;
                var textinput = $('#boxtext');

                textinput.css("position", "absolute").css({"top": topoff + "px", "left": leftoff + "px", "width": percentWidth, height: percentHeight});
                textinput.show();

                textinput.unbind().blur(function () {
                    for (var i = 0; i < $(this).val().length; i++) {
                        console.log($(this).val().charCodeAt(i));
                    }
                    App.storybook.activePage.freeTransform(ng).unplug();
                    console.log("xpos", item.xpos, ng);
                    //ng.remove();
                    var bits = $(this).val().split("\n");
                    var lines = bits.length - 1;
                    var t = App.storybook.activePage.text($(this).position().left, $(this).position().top + ( 12 * lines ) + 10, $(this).val());
                    t.attr({font: "12pt Arial", "text-anchor": "start"});
                    console.log(t);
                    ng.push(t);
                    App.storybook.activePage.freeTransform(ng);
                    $(this).hide();

                });
            }
            App.items.push(ng);
            addItemControls(ng)

            $('.item_choice#' + item.group).trigger("click");

        }
        item.animate({"fill-opacity": 1}, 500);
        item.paper.setSize(item.returnWidth, item.returnHeight);


    }

    item.mousedown(function () {
        item.odx = 0;
        item.ody = 0;
    });


};
function addItemControls(item) {

    //find other items and remove controls add controls to this item

    if (item.type === "text") return;

    for (var it = 0; it < App.items.length; it++) {

        console.log("looking at item " + it, App.items[it]);

        if (item.itemid !== App.items[it].itemid) {
            App.items[it].active = false;

            if (App.items[it].freeTransform) {
                App.items[it].freeTransform.unplug();
            }
        } else {

            if (!App.items[it].active) {

                App.storybook.activePage.freeTransform(App.items[it], {"draw": "bbox"}).showHandles();
                App.items[it].active = true;
            }
        }

    }

    $('.item_controls > .actionbtn').each(function (idx) {
        var raphFunc = $(this).data("function");
        if (item[raphFunc]) {

            $(this).unbind().bind("click", function () {
                var refItem = App.items[item.itemid];
                console.log("calling item control " + raphFunc);
                if (raphFunc == "remove") {
                    App.storybook.activePage.freeTransform(item).unplug();
                    refItem[raphFunc]();
                } else if (raphFunc === "toBack") {
                    if (item && item.itemid && item.itemid !== 0) {
                        var newIndex = item.itemid - 1;
                        var oldIndex = item.itemid;
                        App.items[newIndex].insertAfter(App.items[oldIndex]);
                        App.items[newIndex].itemid += 1;
                        App.items[oldIndex].itemid -= 1;
                        var arrSpl = App.items.splice(oldIndex, 1);
                        App.items.splice(newIndex, 0, arrSpl[0]);
                    }

                } else if ("toFront" === raphFunc) {
                    var newIndex = item.itemid + 1;
                    var oldIndex = item.itemid;
                    App.items[newIndex].insertBefore(App.items[oldIndex]);
                    App.items[newIndex].itemid -= 1;
                    App.items[oldIndex].itemid += 1;
                    var arrSpl = App.items.splice(oldIndex, 1);
                    App.items.splice(newIndex, 0, arrSpl[0]);
                }
                else {
                    if (refItem) {
                        App.storybook.activePage.freeTransform(refItem).unplug();
                        refItem[raphFunc]();
                        App.storybook.activePage.freeTransform(refItem, {"draw": "bbox"}).showHandles();
                    }
                }

            });
        }
    });
    var fab = $.farbtastic('.colour');
    fab.linkTo(function (col) {
        item.attr({"fill": col});
    });
    $('.item_controls').show();


}
function textBox(ele, text) {


}

function textControls(ele) {

    // Retrieve created <input type=text> field
    var input = ele.inlineTextEditing.startEditing();

    input.blur(function (e) {
        // Stop inline editing after blur on the text field
        ele.inlineTextEditing.stopEditing();
        var width = App.storybook.pages["page1"].width;
        var x = (width / 2);
        ele.attr({"x": x});
        var titleText = ele.attr("text");
        if (titleText != "undefined") {
            App.storybook.userContent.title = ele.attr("text");
            App.db.conn.get("book", function (data) {
                if (!data) data = App.storybook.userContent;
                data.title = App.storybook.userContent.title;
                App.db.conn.save({"key": "book", "value": data}, function (done) {
                    console.log("saved title", done);
                });
            });
        }
    });
}

function removeFreeTrans() {
    for (var it in App.items) {
        if (App.items.hasOwnProperty(it)) {
            if (App.items[it].freeTransform) {
                App.items[it].freeTransform.unplug();
            }
        }
    }
}


function populateItems(err, data, item) {
    $('.item_id').each(function (idx, ele) {
        var svg = data.result[0].length > idx ? data.result[0][idx] : undefined;
        if (svg) {
            //svg.svg = svg.svg.replace(/<!--.*-->/,"\n");
            var tmp = $('#tmpsvg');
            tmp.append(svg.svg);
            ele.idx = idx;
            ele.svg = svg.svg;
            ele.group = item;
            App.builder.drawMenuItem(ele, tmp[0], idx);
            tmp.children().remove();

        }
    });
}



