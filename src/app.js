window.App = {};
//App.host = "http://localhost:8080/storybooks/";
App.host = "http://api.storybooks.me/";
App.storybook = {
    "element": "",
    "width": 0,
    "height": 0,
    "pages": {},
    "activePage": "",
    "activeElement": ""
};
App.storybook.userContent = {
    "title": "",
    "content": "",
    "id": undefined,
    "userId": undefined
};
App.db = {
    "name": "storybook",
    "conn": undefined
};
App.menu = {
    "defaultItem": "",
    "defaultHeight": 150
};

App.items = [];
